FAC-COR Report builder
v0.1 updated on 5/25/2021

Setup
1) To generate this report, pull the main repo to your local machine.
2) Update the batch file using a text editor (Notepad or Notepad++) to replace the <change to your username> field as well as the <change to your faccor repo directory> field. 
3) Copy the batch file to an easily accessible location.

Running the report
1) Ensure the current day's FAC-COR Extract.csv report and the previous day's extract (ex. 2021_0524_COR Analysis.xlsx) are in one location. There can be other files in that same folder.
2) Run the FACCOR.bat file
3) Click Browse and navigate to the folder location
4) Click Create Doc to finish the process.
5) The current day's file will be exported to that location with today's date.