#%%
#==========================
#v0.1
#==========================
#Setup
#==========================
import pandas as pd
pd.options.mode.chained_assignment = None
from datetime import date, timedelta
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog

test = 0



#%%
#==========================
#parameter setup
#==========================
report_date = date.today().strftime("%m/%d/%Y")
report_month = date.today().strftime('%m')
report_year = date.today().strftime('%y')
filename = f'{date.today().strftime("%Y_%m%d")}_COR Analysis'

#Quarter calculator
if 10 < int(report_month) <= 12:
    report_quarter = 'Q1'
elif 1 < int(report_month) <= 3:
    report_quarter = 'Q2'
elif 4 < int(report_month) <= 6:
    report_quarter = 'Q3'
else:
    report_quarter = 'Q4'

#FY calculator
if report_month == 'Q1':
    report_fy = str(int(date.today().strftime('%y'))+1)
else:
    report_fy = date.today().strftime('%y')

#Period calculator
report_period = str(int(report_month) + 2)

#Friday calculator
friday = date.today() - timedelta(days=3)
yesterday = date.today() - timedelta(days=1)



#%%
#==========================
#tkinter setup
#==========================
#if test == 0:
main = tk.Tk()
main.geometry('400x150') 
main.title('FAC-COR File Builder')

#frame
frame = tk.Frame(main,padx=5, pady=5, width=400, height=150)

#style
frame.grid_rowconfigure(0, minsize=30)
frame.grid_rowconfigure(1, minsize=30)
frame.grid_rowconfigure(2, minsize=45)

style = ttk.Style()
style.configure('BWTLabel', foreground="black", background="white")
ttk.Style().configure("TButton", padding=6, relief="flat",   background="#ccc")

#functions
#get path from user, set label 
def getFilePath():
    global fpath
    fpath = filedialog.askdirectory()
    tk.Label(frame, text=fpath,justify=tk.LEFT, wraplength=250).grid(row=0, column = 1) 
def closeUI():
    main.destroy()

#file location
lblpath = tk.Label(frame, text= 'File Location:').grid(row=0, column = 0, sticky= tk.SW) 
btnpath = ttk.Button(frame, text='Browse...', command=getFilePath, width=16).grid(row=1, column = 0, sticky= tk.SW) 

btnCreate = ttk.Button(frame, text = 'Create Doc', command=closeUI, width=16).grid(row=2, column=0, sticky= tk.SW)
frame.grid(row=0,column=0)
main.mainloop()



#%%
#==========================
#==========================
#Test Setup Section
if test == 1:
    new_fac_cor_path = 'C:/Users/eharting/Documents/Programming/FAC_COR/FAC-COR Extract.csv'
    old_fac_cor_path = 'C:/Users/eharting/Documents/Programming/FAC_COR/2021_0521_COR Analysis.xlsx'
    export_path = f'C:/Users/eharting/Documents/Programming/FAC_COR/{filename}_test.xlsx'
else:
    new_fac_cor_path = f'{fpath}/FAC-COR Extract.csv'
    if date.weekday(date.today()) == 0:
        old_fac_cor_path = f'{fpath}/{friday.strftime("%Y_%m%d")}_COR Analysis.xlsx'
    else:
        old_fac_cor_path = f'{fpath}/{yesterday.strftime("%Y_%m%d")}_COR Analysis.xlsx'
    export_path =f'{fpath}/{filename}.xlsx'
#==========================
#==========================
    
    
    
#%%
#==========================
#Dataframe and Excel Setup
#==========================
writer = pd.ExcelWriter(export_path, engine='xlsxwriter')
workbook = writer.book

#new_fac_cor dataframe
new_fac_cor = pd.read_csv(new_fac_cor_path, encoding='utf-16 le', sep='\t', na_values='test', skiprows=[1]).iloc[:, :-3]
new_fac_cor = new_fac_cor[new_fac_cor['COR_Email'].notnull()]

level = new_fac_cor.copy()
level = level[level['COR_Email'].notnull()]

if test == 1:
    inactive = pd.read_excel(old_fac_cor_path, '20210521')
    new = pd.read_excel(old_fac_cor_path, '20210521')
    old_fac_cor = pd.read_excel(old_fac_cor_path, '20210521')
elif date.weekday(date.today()) == 0:
    inactive = pd.read_excel(old_fac_cor_path, str(f'{friday.strftime("%Y%m%d")}'))
    new = pd.read_excel(old_fac_cor_path, str(f'{friday.strftime("%Y%m%d")}'))
    old_fac_cor = pd.read_excel(old_fac_cor_path, str(f'{friday.strftime("%Y%m%d")}'))
else:
    inactive = pd.read_excel(old_fac_cor_path, str(f'{yesterday.strftime("%Y%m%d")}'))
    new = pd.read_excel(old_fac_cor_path, str(f'{yesterday.strftime("%Y%m%d")}'))
    old_fac_cor = pd.read_excel(old_fac_cor_path, str(f'{yesterday.strftime("%Y%m%d")}'))
    

def get_col_widths(df):
    # First we find the maximum length of the index column   
    idx_max = max([len(str(s)) for s in df.index.values] + [len(str(df.index.name))])
    # Then, we concatenate this to the max of the lengths of column name and its values for each column, left to right
    return [idx_max] + [max([len(str(s)) for s in df[col].values] + [len(col)]) for col in df.columns]



#%%
#==========================
#Inactive Worksheet
#==========================
inactive = inactive[~inactive['COR_Email'].isin(list(new_fac_cor['COR_Email']))]
inactive['Match In New Extract?'] = 'No'

inactive.to_excel(writer, sheet_name='Inactive', index=False)
inactive_sheet = writer.sheets['Inactive']


for i, width in enumerate(get_col_widths(inactive)):
    inactive_sheet.set_column(i-1, i-1, width)


#%%
#==========================
#New Worksheet
#==========================
new = new_fac_cor[~new_fac_cor['COR_Email'].isin(list(old_fac_cor['COR_Email']))]
new['Match In Old Extract?'] = 'No'

new.to_excel(writer, sheet_name='New', index=False)
new_sheet = writer.sheets['New']


for i, width in enumerate(get_col_widths(new)):
    new_sheet.set_column(i-1, i-1, width)


#%%
#==========================
#Level Worksheet
#==========================
level.insert(9,'Old Level','')
level['Old Level']=new_fac_cor['Level']

level.insert(10,'Match?','')
level['Match?']=new_fac_cor['Level'].eq(level['Level'])

level = level[level['Match?'] == 'True']

level.to_excel(writer, sheet_name='Level', index=False)
level_sheet = writer.sheets['Level']

for i, width in enumerate(get_col_widths(level)):
    level_sheet.set_column(i-1, i-1, width)



#%%
#==========================
#writing Old Worksheet
#==========================
if date.weekday(date.today()) == 0:
    old_fac_cor.to_excel(writer, sheet_name=str(f'{friday.strftime("%Y%m%d")}'), index=False)
    old_fac_cor_sheet = writer.sheets[str(f'{friday.strftime("%Y%m%d")}')]
else:
    old_fac_cor.to_excel(writer, sheet_name=str(f'{yesterday.strftime("%Y%m%d")}'), index=False)
    old_fac_cor_sheet = writer.sheets[str(f'{yesterday.strftime("%Y%m%d")}')]
    
for i, width in enumerate(get_col_widths(old_fac_cor)):
    old_fac_cor_sheet.set_column(i-1, i-1, width)



#%%
#==========================
#Writing New Worksheet
#==========================
new_fac_cor.to_excel(writer, sheet_name=str(f'{date.today().strftime("%Y%m%d")}'), index=False)
new_fac_cor_sheet = writer.sheets[str(f'{date.today().strftime("%Y%m%d")}')]

for i, width in enumerate(get_col_widths(old_fac_cor)):
    new_fac_cor_sheet.set_column(i-1, i-1, width)



#%%
writer.save()